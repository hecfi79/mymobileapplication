package com.example.myapplication;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

public class MyViewModel extends ViewModel {
    private MutableLiveData<ArrayList<Adapter.Params>> forecast;

    public LiveData<ArrayList<Adapter.Params>> getForecast() {
        if (forecast == null) {
            forecast = new MutableLiveData<>();
            forecast.setValue(new ArrayList<Adapter.Params>());
        }
        return forecast;
    }

    public void setForecast(ArrayList<Adapter.Params> params) {
        forecast.setValue(params);
    }
}
