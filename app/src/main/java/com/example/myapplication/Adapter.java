package com.example.myapplication;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder>{

    public static class Params {
        public String main_description;
        public Integer temperature;
    }

    private final ArrayList<Params> states;

    Adapter(ArrayList<Params> states) { this.states = states; }

    @NonNull
    @Override
    public Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.forecast, parent, false);
        return new ViewHolder(view);
    }

    public ArrayList<Params> getParamsList() {
        return states;
    }

    @Override
    public void onBindViewHolder(Adapter.ViewHolder holder, int position) {
        holder.setParams(states.get(position));
    }

    @Override
    public int getItemCount() {
        return states.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView img_forecast;
        private final TextView text_forecast;

        ViewHolder(View view){
            super(view);
            img_forecast = view.findViewById(R.id.img_forecast);
            text_forecast = view.findViewById(R.id.text_forecast);
        }

        @SuppressLint("SetTextI18n")
        public void setParams(Params state) {
            text_forecast.setText(state.temperature);
            if(state.main_description.equals("Clouds"))
                img_forecast.setImageResource(R.drawable.clouds);
            if(state.main_description.equals("Snow"))
                img_forecast.setImageResource(R.drawable.snow);
            if(state.main_description.equals("Drizzle"))
                img_forecast.setImageResource(R.drawable.drizzle);
            if(state.main_description.equals("Atmosphere"))
                img_forecast.setImageResource(R.drawable.atmosphere);
            if(state.main_description.equals("Clear"))
                img_forecast.setImageResource(R.drawable.clear);
            if(state.main_description.equals("Thunderstorm"))
                img_forecast.setImageResource(R.drawable.thunderstorm);
            if(state.main_description.equals("Rain"))
                img_forecast.setImageResource(R.drawable.rain);
            text_forecast.setText(state.temperature + "*C");
        }
    }
}
