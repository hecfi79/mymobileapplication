package com.example.myapplication;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface APIBuilder {
    @GET("data/2.5/weather?appid=14d39398cffc85b3076e47f93fc06bae&units=metric")
    Call<JsonObject> getWeather(@Query("q") String town);

    @GET("data/2.5/forecast?&units=metric&appid=14d39398cffc85b3076e47f93fc06bae")
    Call<JsonObject> getForecast(@Query("q") String town);
}
