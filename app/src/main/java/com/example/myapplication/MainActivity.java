package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private TextView logo, resultinfo, temperature;
    private EditText town;
    private Button getweather;
    private ImageView description;
    private APIBuilder apiBuilder;
    private RecyclerView recyclerView;
    private Adapter adapter;
    private MyViewModel viewModel;
    private ArrayList<Adapter.Params> params;

    public TextView getLogo() {
        return this.logo;
    }

    public TextView getResultinfo() {
        return this.resultinfo;
    }

    public EditText getTown() {
        return this.town;
    }

    public Button getButton() {
        return this.getweather;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recycle);
        viewModel = new ViewModelProvider(this).get(MyViewModel.class);
        viewModel.getForecast().observe(this, t -> {
            LinearLayoutManager manager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(manager);
            recyclerView.setHasFixedSize(true);
            params = t;
            adapter = new Adapter(new ArrayList<Adapter.Params>());
            recyclerView.setAdapter(adapter);
        });

        town = findViewById(R.id.town);
        logo = findViewById(R.id.logo);
        resultinfo = findViewById(R.id.resultinfo);
        getweather = findViewById(R.id.getweather);
        description = findViewById(R.id.description);
        temperature = findViewById(R.id.temperature);

        retrofitWeatherAPI();

        getweather.setOnClickListener(view -> {
            getNowWeather();
            getForecast();
        });
    }

    private void retrofitWeatherAPI() {
        String url = "https://api.openweathermap.org/";
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create(gson)).build();
        apiBuilder = retrofit.create(APIBuilder.class);
    }

    private void getNowWeather() {
        apiBuilder.getWeather(town.getText().toString()).enqueue(new GetRequest());
    }

    private void getForecast() {
        apiBuilder.getForecast(town.getText().toString()).enqueue(new GetForecast());
    }

    private class GetForecast implements Callback<JsonObject> {
        private JSONObject forecastWeather;

        @SuppressLint("SetTextI18n")
        @Override
        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
            if(response.isSuccessful()) {
                try {
                    assert response.body() != null;
                    forecastWeather = new JSONObject(response.body().toString());
                    JSONArray forecast = forecastWeather.getJSONArray("list");
                    params = new ArrayList<>();
                    for (int i = 0; i < forecast.length(); i++) {
                        Adapter.Params param = new Adapter.Params();
                        param.temperature = Math.toIntExact(Math.round(forecast.getJSONObject(i).getJSONObject("main").getDouble("temp")));
                        param.main_description = forecast.getJSONObject(i).getJSONArray("weather").getJSONObject(0).getString("main");
                        params.add(param);
                    }
                    viewModel.setForecast(params);
                } catch (JSONException e) { e.printStackTrace(); }
            } else resultinfo.setText("error");
        }

        @Override
        public void onFailure(Call<JsonObject> call, Throwable t) {}
    }

    private class GetRequest implements Callback<JsonObject> {
        private JSONObject nowWeather;

        @SuppressLint("SetTextI18n")
        @Override
        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
            if(response.isSuccessful()) {
                try {
                    assert response.body() != null;
                    nowWeather = new JSONObject(response.body().toString());
                    double lon = nowWeather.getJSONObject("coord").getDouble("lon"), lat = nowWeather.getJSONObject("coord").getDouble("lat");
                    resultinfo.setText(
                        "Temperature: " + nowWeather.getJSONObject("main").getDouble("temp") + "*C\n" +
                        "Description: " + nowWeather.getJSONArray("weather").getJSONObject(0).getString("description") + "\n" +
                        "Feels like: " + nowWeather.getJSONObject("main").getDouble("feels_like") + "*C\n" +
                        "Min. temperature: " + nowWeather.getJSONObject("main").getDouble("temp_min") + "*C\n" +
                        "Max. temperature: " + nowWeather.getJSONObject("main").getDouble("temp_max") + "*C\n" +
                        "Pressure: " + nowWeather.getJSONObject("main").getDouble("pressure") + "\n" +
                        "Humidity: " + nowWeather.getJSONObject("main").getDouble("humidity") + "\n" +
                        "Visibility: " + nowWeather.getInt("visibility") + "\n" +
                        "Wind speed: " + nowWeather.getJSONObject("wind").getDouble("speed") + "\n" +
                        "Wind deg: " + nowWeather.getJSONObject("wind").getInt("deg") + "\n" +
                        "Wind gust: " + nowWeather.getJSONObject("wind").getInt("gust")
                    );
                    if(nowWeather.getJSONArray("weather").getJSONObject(0).getString("main").equals("Clouds"))
                        description.setImageResource(R.drawable.clouds);
                    if(nowWeather.getJSONArray("weather").getJSONObject(0).getString("main").equals("Snow"))
                        description.setImageResource(R.drawable.snow);
                    if(nowWeather.getJSONArray("weather").getJSONObject(0).getString("main").equals("Drizzle"))
                        description.setImageResource(R.drawable.drizzle);
                    if(nowWeather.getJSONArray("weather").getJSONObject(0).getString("main").equals("Atmosphere"))
                        description.setImageResource(R.drawable.atmosphere);
                    if(nowWeather.getJSONArray("weather").getJSONObject(0).getString("main").equals("Clear"))
                        description.setImageResource(R.drawable.clear);
                    if(nowWeather.getJSONArray("weather").getJSONObject(0).getString("main").equals("Thunderstorm"))
                        description.setImageResource(R.drawable.thunderstorm);
                    if(nowWeather.getJSONArray("weather").getJSONObject(0).getString("main").equals("Rain"))
                        description.setImageResource(R.drawable.rain);
                    temperature.setText(nowWeather.getJSONObject("main").getDouble("temp") + "*C");
                } catch (JSONException e) { e.printStackTrace(); }
            } else resultinfo.setText("error");
        }

        @Override
        public void onFailure(Call<JsonObject> call, Throwable t) {}
    }
}